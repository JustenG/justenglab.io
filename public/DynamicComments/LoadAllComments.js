var commentCount = 1;
var count = 0;
var loaded = false;

window.onload = function() {
    RefreshComments();
};

window.setInterval(function(){
    if(loaded)
        RefreshComments();
}, 5000);

function GetCell(range)
{
    var output = "https://sheets.googleapis.com/v4/spreadsheets/1YKpCJKJq9PozTSSy8AUJOlJ23ql0nsh_vQlSRp-lQuM/values/Form%20Responses%201!" + range + "?key=AIzaSyAOcDa5pPmEtjx3Zl6dleNK43iCPD3X01Q"
    return output;
}

function GetValue(data)
{
    return Object.values(data)[2];
}

function RefreshComments()
{
    $.get(GetCell("C1"), function(data, textStatus) {
        count = GetValue(data);
        
        if(commentCount > count)
            return;
        
        GetNextComment(count);
        
    });
    console.log("Number of comment is: " + count);

}

function GetNextComment(i)
{
    $.get(GetCell("B" + (1+commentCount)), function(dataComment, textStatus) {
        if(GetValue(dataComment) != "undefined" && GetValue(dataComment) != "");
        {
            $.get(GetCell("A" + (1+commentCount)), function(dataDate, textStatus) {
                var div = document.createElement("div");
                div.id = "comment";
                div.innerHTML = "<br>" + GetValue(dataComment) + "<br><br> <p style='font-size:12px' color='gray'>Posted: "  + GetValue(dataDate)+ "<p/><br>" ;
                document.getElementById("allcomments").appendChild(div);
                var hr = document.createElement("hr");
                hr.id = "commentBreak";
                document.getElementById("allcomments").appendChild(hr);
                
                commentCount++;
                
                if(commentCount <= i)
                    GetNextComment(i);
                else
                    loaded = true;
            });
        }
    });
}